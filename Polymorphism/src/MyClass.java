public class MyClass {

    public static void main(String[] args){
        /*
        Employee is difference variable
        e is the reference name
        FullTime is the type of object
        */
        Employee e = new FullTime();
        System.out.println("Full time employee salary is: " + e.salary());

        /* static method like designation does not bind in
        with the instance of a class. It directly binds with the class*/
        //e.designation();

        // Uses Class name to access the static methods
        System.out.println(Employee.designation());
        System.out.println(FullTime.designation());
        System.out.println(Contractor.designation());

        /*
           myMethod is only available on the full time class
         */
        FullTime f = new FullTime();
        f.myMethod();

        e = new Contractor();
        System.out.println("Contractor employee salary is: " + e.salary());

        /*
        * Printing Overloading Methods
        * */

        MyOverloadingClass myOverloadingClass = new MyOverloadingClass();
        myOverloadingClass.myMethod(5);
        myOverloadingClass.myMethod("My String");
        myOverloadingClass.myMethod("My String", 5);
    }
}
