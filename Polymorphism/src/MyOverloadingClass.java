public class MyOverloadingClass {

    /*
    * Overloading is when you have multiple method
    * with the same name, but has different parameters
    * or argument
    * */

    public void myMethod (String str){
        System.out.println(str);
    }

    public void myMethod (String str, int i){
        System.out.println(str + i);
    }

    public void myMethod (int i){
        System.out.println(i);
    }
}
