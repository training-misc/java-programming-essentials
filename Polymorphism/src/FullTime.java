public class FullTime extends Employee{

    /* Override the method of salary from Employee*/
    @Override
    int salary() {
        return base + 20000;
    }

    void myMethod(){
        System.out.println("This Method is only available in Full Time Class");
    }

    /* Bonus method cannot be overwritten because its final
        @Override
        int bonus(){
            return bonus;
        }
     */

    static String designation(){
        return "full time";
    }
}
