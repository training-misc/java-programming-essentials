public class MYClass {

    enum myChar {
        A,
        B
    }
    public static void main (String[] args){
        MYClass myClass = new MYClass();
        myClass.switchCheck();
    }

    public void ifElseCheck(){
        boolean flag = true;
        String str = "myStr";

        if(str.equalsIgnoreCase("myStr")){
            System.out.println("Im in if");
        } else {
            System.out.println("Im in else");
        }
    }

    public void forLoopCheck(){

        for(int i = 0; i < 5; i++){
            System.out.println("I is " + i);
            if(i == 3){
                break;
            }
        }
    }

    public void forEachLoopCheck() {
        String[] myArray = {"a", "b", "c"};

        for (String str : myArray){
            System.out.println(str);
            if(str.equalsIgnoreCase("b")){
                break;
            }
        }
    }

    public void whileLoopCheck(){

        int i = 5;

        while (i > 0){
            System.out.println("i is " + i);
            i--;
            if(i == 3){
                break;
            }
        }
    }

    public void switchCheck(){
        String str = "ab";
        myChar nowChar = myChar.A;

        switch (nowChar){
            case A:
                System.out.println(nowChar);
                break;
            case B:
                System.out.println(nowChar);
                break;
        }
    }
}
