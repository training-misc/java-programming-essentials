package qa;

public class QaClass {
    public String str = "My String";
    private String strQA = "This is a private attribute";
    protected String strProtected = "This is a protected attribute";

    private void myPrivateMethod(){
        System.out.println("My private method in qa class!");
    }

    public void myMethod(){
        System.out.println("qa class my method");
    }

    void  onlyAccessQAFolder(){
        System.out.println("This can only be access in the qa folder");
    }

    private void accessPrivateMethod(){
        myPrivateMethod();
        System.out.println(strQA);
    }

    public void myProtectedMethod(){
        System.out.println("qa class my protected method");
    }
}
