package qa;

public class QaClass1 {

    public void myQaMethod1(){
        QaClass qaClass = new QaClass();
        qaClass.onlyAccessQAFolder(); // Can access qa class because its the same qa folder
    }

    public void myQaMethod2(){
        QaClass qaClass = new QaClass();
        // qaClass.myPrivateMethod(); < -- Only accessible within the class
    }

    public void myQaMethod3(){
        QaClass qaClass = new QaClass();
        System.out.println(qaClass.strProtected);
        qaClass.myProtectedMethod(); //< -- Only accessible within the class
    }
}
