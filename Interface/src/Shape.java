interface Shape {
    int i = 5;

    String color();
    double area();
    String info();

    default void defaultMethod(){
        System.out.println("default method");
    }

    static void staticMethod(){
        System.out.println("static method");
    }
}
