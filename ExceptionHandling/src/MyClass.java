import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyClass {

    public static void main (String[] arg)  {
        /* Run Time Example -During execution*/
        // int myArray[] ={1,2,3};
        // System.out.println(myArray[3]);
        // Index 3 out of bounds for length 3


        //	at MyClass.main(MyClass.java:5)
        /* Compile Time Example */
        //File does not exist
        //File file = new File("../myFile.txt");
        //FileReader fr = new FileReader(file);
        // Unhandled exception: java.io.FileNotFoundException

        // public static void main (String[] arg) throws FileNotFoundException
        // means not handling the exception, but postponing the exception
        // during run time
        File file = new File("../myFile.txt");
        FileReader fr = null;
        try {
            fr = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace(); //exits the program with success
        } finally {
            try{
                if(fr != null){
                    fr.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        System.out.println("after exception");

        /* Alternative
        * File file = new File("../myFile.txt");
        * try(FileReader fr = new FileReader(file)){
        * } catch (IOException e) {
        *   e.printStackTrace();
        * }
        * System.out.println("after exception");
        *
        * */

        /* Alternative
         * File file = new File("../myFile.txt");
         * try(){
         *  FileReader fr = new FileReader(file);
         *  throw new MyException();
         * } catch (MyException e){
         *   e.printException();
         *   e.printStackTrace();
         * }
         * System.out.println("after exception");
         *
         * */

    }
}
