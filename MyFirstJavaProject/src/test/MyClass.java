package test;

public class MyClass {
    //fields - parameters - variables - int, float, double, boolean, string
    // constructors
    // methods

    // static final variable cannot be overwritten
    int i = 5 ; //instance variable
    String str = "Hello World";

    public MyClass() {
        // constructor
    }

    public static void main(String[] args){
        MyClass myClass = new MyClass();
        myClass.stringCheck();
    }

    public void stringCheck(){
        str = str.concat(" !!");
        System.out.println(str);
        str = str.toUpperCase();
        System.out.println(str);

//        str = str.substring(6);
//        System.out.println(str);

        str = str.substring(6,11);
        System.out.println(str);

        boolean flag = str.equalsIgnoreCase("WORLD");
        System.out.println(flag);

        str = "5";
        int i = Integer.parseInt(str);
        System.out.println(i);

        i = 6;
        str = String.valueOf(i);
        System.out.println(str);
    }

    public MyClass  initialize() {
        i = 6;
        return this;
    }
}
