public class MyClass {

    public static void main(String[] args){
        MyPrivateClass myPrivateClass = new MyPrivateClass();
        /* Write and Read */
        myPrivateClass.setStr("My String");
        System.out.println(myPrivateClass.getStr());

        /* Write only */
        myPrivateClass.setName("My Name is John Doe");
        myPrivateClass.printName();
    }
}
