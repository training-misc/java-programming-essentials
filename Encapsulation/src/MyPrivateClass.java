public class MyPrivateClass {
    private  String str ="My String";

    // Save the state of the variable
    private static  String name =" John Doe";

    /*
    * Read and Write
    * */
    public String getStr(){
        return str;
    }

    public void setStr(String str){
        this.str = str;
    }

    private void myMethod(){
        System.out.println("my method");
    }

    /*
    * Write only
    * */

    public void setName(String name) {
        this.name = name;
    }

    public void printName(){
        System.out.println(name);
    }
}
