public class MySuperClass {
    String superStr = "Super Class String";
    String commonStr = "super common string";

    public MySuperClass(String constructorStr){
        System.out.println("Super Class Constructor");
        System.out.println(constructorStr);
    }


    public void superClassMethod(){
        System.out.println("Super Class method");
    }

    public void commonMethod(){
        System.out.println("Super common method");
    }
}
