import java.io.*;

public class MyClass {

    public static void main(String[] args){

        String dirPath = "folder" + File.separator + "anotherFolder";
        File dir = new File(dirPath);

        if(!dir.exists()){
            dir.mkdirs();
        }
        /* Creates a file*/
        File file = new File(dirPath + File.separator + "MyFile.txt");

        /* Checks if file has been created */
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println(file.getName());
        System.out.println(file.getAbsoluteFile());
        System.out.println("Write the file: " + file.canWrite());
        System.out.println("Read the file: " + file.canRead());

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            String str = "My String";
            outputStream.write(str.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            int i;
            while ((i = fileInputStream.read()) != -1){
                System.out.print((char) i);
            }
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        file.delete();
//
//        /* Write inside the file*/
//        try {
//            FileWriter fileWriter = new FileWriter(file);
//            fileWriter.write("first line");
//            fileWriter.flush();
//            fileWriter.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        /* Reads file*/
//        try {
//            FileReader fileReader = new FileReader(file);
//            int i;
//            while ((i = fileReader.read()) != -1){
//                System.out.print((char) i);
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }
}
